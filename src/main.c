#include <util/delay.h>
#include <stdlib.h>
#include <string.h>

#include "bmp280.h"
#include "fat16.h"
#include "usart.h"
#include "sd.h"
#include "spi.h"
#include "shell.h"
#include "twi.h"

int main(void) {
    USART_init(115200);
    SHELL_init();
    // SPI_init();
    // SD_init();
    // FAT16_init();
    TWI_init();
    BMP_init();

    BMP_config(BMP280_OSRS_P_16, BMP280_OSRS_T_2, BMP280_MODE_NORMAL, BMP280_FILTER_16);
    BMP_load_comp_values();

    //SHELL_loop();
    while(1) {
        USART_sendNumber(BMP_get_press());
        USART_sendChar('\n');
        for(int i = 0; i < 1e4; i++) {}
    }
}
