#ifndef SD_H
#define SD_H

#include <stdint.h>

uint8_t SD_buffer[512];

void SD_init();
void SD_readBlock(int n);
void SD_writeBlock(uint32_t n);

#endif // SD_H
