#include <stdlib.h>
#include <string.h>

#include "usart.h"
#include "sd.h"
#include "shell.h"
#include "spi.h"

#define BLOCK_SIZE 512

int cmd() {
    char* mode = strtok(NULL, SHELL_DELIM);
    if(strcmp(mode, "r") == 0) {
        char* arg = strtok(NULL, SHELL_DELIM);
        uint16_t byte = 0;
        int block = (int)strtol(arg, NULL, 0);
        SD_readBlock(block);
        USART_sendString("     |");
        for(int i = 0; i < 16; i++) {
            USART_sendChar(' ');
            USART_sendHex(i);
        }
        USART_sendString(" |\n-----+---------------------------------------------------------------------------------+-----------------\n");
        for(int r = 0; r < 32; r++) {
            USART_sendHex(byte >> 4);
            USART_sendString(" |");
            for(int c = 0; c < 16; c++) {
                USART_sendChar(' ');
                USART_sendHex(SD_buffer[r * 16 + c]);
                byte++;
            }
            USART_sendString(" | ");
            for(int c = 0; c < 16; c++) {
                char a = SD_buffer[r * 16 + c];
                if(a >= 32 && a <= 126) USART_sendChar(a);
                else USART_sendChar('.');
            }
            USART_sendChar('\n');
        }
    } else {
        USART_sendString("Unknown sd command.\n");
        return 2;
    }
    return 0;
}

uint8_t SD_command(uint8_t cmd, uint32_t arg, uint8_t crc) {
    uint8_t resp;
    cmd |= 0x40;

    SD_ENABLE();

    SPI_write(cmd);
    SPI_write(arg>>24);
    SPI_write(arg>>16);
    SPI_write(arg>>8);
    SPI_write(arg>>0);
    SPI_write(crc);

    do {
        resp = SPI_write(0xFF);
    } while(resp == 0xFF);

    SD_DISABLE();

    return resp;
}

void SD_init() {
    uint8_t i, resp;

    SD_DISABLE();
    for(i = 0; i < 10; i++)
        SPI_write(0xFF);

    resp = SD_command(0, 0x00000000, 0x95);
    if(resp != 0x01) USART_sendString("SD error on CMD0\n");

    do {
        resp = SD_command(1, 0x00000000, 0xFF);
    } while(resp != 0x00);

    SD_command(16, 0x00000200, 0xFF);

    USART_sendString("SD initalized\n");

    SHELL_register("sd", &cmd);
}

void SD_readBlock(int n) {
    /*
    USART_sendString("SD read block ");
    USART_sendNumber(n);
    USART_sendString("\n");
    */
    uint8_t resp = SD_command(17, (uint32_t)n * BLOCK_SIZE, 0xFF);
    if(resp != 0x00) {
        SD_DISABLE();
        USART_sendString("SD data error on read block\nresp := ");
        USART_sendHex(resp);
        USART_sendChar('\n');
        return;
    }

    SD_ENABLE();

    while((resp = SPI_write(0xFF)) != 0xFE) {
        if(!(resp & 0xF0)) {
            SD_DISABLE();
            USART_sendString("SD data error on read block\nresp := ");
            USART_sendHex(resp);
            USART_sendChar('\n');
            return;
        }
    }

    for(int i = 0; i < BLOCK_SIZE; i++)
        SD_buffer[i] = SPI_write(0xFF);

    // Checksum
    SPI_write(0xFF);
    SPI_write(0xFF);

    SD_DISABLE();
}

void SD_writeBlock(uint32_t n) {
    uint8_t resp = SD_command(24, (uint32_t)n * BLOCK_SIZE, 0xFF);
    if(resp != 0x00) {
        SD_DISABLE();
        USART_sendString("SD data error on write block\nresp := ");
        USART_sendHex(resp);
        USART_sendChar('\n');
        return;
    }

    SD_ENABLE();

    SPI_write(0xFE);
    for(int i = 0; i < BLOCK_SIZE; i++)
        SPI_write(SD_buffer[i]);

    // Checksum
    SPI_write(0xFF);
    SPI_write(0xFF);

    resp = SPI_write(0xFF);
    if(resp & 0x05)
        USART_sendString("Write complete\n");
    else if(resp & 0xB)
        USART_sendString("CRC error on write\n");
    else if(resp & 0xD)
        USART_sendString("Write error\n");

    while((resp = SPI_write(0xFF)) == 0x00) {;}

    SD_DISABLE();
}
