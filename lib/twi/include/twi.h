#ifndef TWI_H
#define TWI_H

#include <avr/io.h>
#include <stdint.h>

void TWI_init();
uint8_t TWI_start(uint8_t addr, uint8_t rw);
uint8_t TWI_write(uint8_t data);
uint8_t TWI_read(uint8_t nack);
void TWI_stop();

#endif // TWI_H
