#include "twi.h"
#include "usart.h"

void TWI_init() {
    PORTMUX.TWISPIROUTEA |= PORTMUX_TWI0_ALT1_gc;
    PORTA.PIN3CTRL |= PORT_PULLUPEN_bm; PORTA.PIN2CTRL |= PORT_PULLUPEN_bm;
    TWI0.MBAUD = (uint8_t)((((float)F_CPU / (float)100000 / 6) - 10) / 2);
    TWI0.MCTRLA |= TWI_ENABLE_bm;
    TWI0.MSTATUS |= TWI_BUSSTATE_IDLE_gc;
    USART_sendString("TWI initalized\n");
}

uint8_t TWI_start(uint8_t addr, uint8_t rw) {
    TWI0.MADDR = (addr << 1) + rw;
    while(!(TWI0.MSTATUS & (TWI_WIF_bm | TWI_RIF_bm)));
    if(TWI0.MSTATUS & (TWI_ARBLOST_bm | TWI_BUSERR_bm)) return 0;
    return !(TWI0.MSTATUS & TWI_RXACK_bm);
}

uint8_t TWI_write(uint8_t data) {
    TWI0.MDATA = data;
    while(!(TWI0.MSTATUS  & TWI_WIF_bm));
    if(TWI0.MSTATUS & (TWI_ARBLOST_bm | TWI_BUSERR_bm)) return 0;
    return !(TWI0.MSTATUS & TWI_RXACK_bm);
}

uint8_t TWI_read(uint8_t nack) {
    while(!(TWI0.MSTATUS  & TWI_RIF_bm));
    uint8_t data = TWI0.MDATA;
    if(nack) TWI0.MCTRLB |= TWI_ACKACT_bm;
    else TWI0.MCTRLB &= ~TWI_ACKACT_bm;
    TWI0.MCTRLB |= TWI_MCMD_RECVTRANS_gc;

    return data;
}

void TWI_stop() {
    TWI0.MCTRLB |= TWI_MCMD_STOP_gc;
}
