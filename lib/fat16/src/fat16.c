#include <stdlib.h>
#include <string.h>

#include "fat16.h"
#include "shell.h"
#include "sd.h"
#include "usart.h"

uint32_t dirCluster = 0;

int ls() {
    SD_readBlock(FAT16_rootSector + dirCluster * BootSector.sectors_per_cluster);
    for(uint32_t i = 0; i < 16; i++) {
        memcpy(&Entry, SD_buffer + sizeof(Entry) * i, sizeof(Entry));
        FAT16_printEntry();
    }
    return 0;
}

void toUpper(char* str) {
    for(int i = 0; str[i] != '\0'; i++) {
        if(str[i] >= 'a' && str[i] <= 'z')
            str[i] -= 'a' - 'A';
    }
}

void getName(char* name) {
    memset(name, ' ', 8);
    char* arg = strtok(NULL, SHELL_DELIM);
    if(strlen(arg) > 8) {
        USART_sendString("Name to long\n");
        return;
    }
    for(int i = 0; arg[i] != '\0'; i++) {
        name[i] = arg[i];
    }

    toUpper(name);
}

int cd() {
    char name[8];
    getName(name);
    SD_readBlock(FAT16_rootSector + dirCluster * BootSector.sectors_per_cluster);
    for(uint32_t i = 0; i < 16; i++) {
        memcpy(&Entry, SD_buffer + sizeof(Entry) * i, sizeof(Entry));
        if((Entry.attributes & 0x10) && (memcmp(name, Entry.filename, 8) == 0)) {
            USART_flush();
            dirCluster = Entry.starting_cluster;
            if(dirCluster != 0) dirCluster--;
            return 0;
        }
    }
    USART_sendString("Not a directory\n");
    return 1;
}

int cat() {
    char name[8];
    getName(name);
    SD_readBlock(FAT16_rootSector + dirCluster * BootSector.sectors_per_cluster);
    for(uint32_t i = 0; i < 16; i++) {
        memcpy(&Entry, SD_buffer + sizeof(Entry) * i, sizeof(Entry));
        if(((unsigned char)Entry.filename[0] != 0xE5) && (memcmp(name, Entry.filename, 8) == 0)) {
            int fileCluster = Entry.starting_cluster - 1;
            SD_readBlock(FAT16_rootSector + fileCluster * BootSector.sectors_per_cluster);
            USART_sendStringN((char*)SD_buffer, Entry.file_size);
            return 0;
        }
    }
    USART_sendString("Not a file\n");
    return 1;
}

void FAT16_init() {
    FAT16_readBoot();

    FAT16_rootSector = PartitionTable.start_sector + BootSector.reserved_sectors + BootSector.fat_size_sectors * BootSector.number_of_fats;

    USART_sendString("Found partition [");
    USART_sendStringN(BootSector.volume_label, sizeof(BootSector.volume_label));
    USART_sendString("]\n");

    SHELL_register("ls", &ls);
    SHELL_register("cd", &cd);
    SHELL_register("cat", &cat);
}

void FAT16_readBoot() {
    SD_readBlock(0);
    memcpy(&PartitionTable, SD_buffer + 0x1BE, 16);
    SD_readBlock(PartitionTable.start_sector);
    memcpy(&BootSector, SD_buffer, 512);
}

void FAT16_printEntry() {
    if(Entry.attributes == 0x0F) return;
    switch((unsigned char)Entry.filename[0]) {
        case 0x00: return;
        case 0xE5:
            USART_sendString("Deleted file: [?");
            USART_sendStringN(Entry.filename+1, 7);
            USART_sendStringN(Entry.ext, 3);
            USART_sendString("]\n");
            return;
        case 0x05:
            USART_sendString("File starting with 0xE5: [");
            USART_sendChar(0xE5);
            USART_sendStringN(Entry.filename+1, 7);
            USART_sendStringN(Entry.ext, 3);
            USART_sendString("]\n");
            break;
        case 0x2E:
            USART_sendString("Directory: [");
            USART_sendStringN(Entry.filename, 8);
            USART_sendStringN(Entry.ext, 3);
            USART_sendString("]\n");
            break;
        default:
            USART_sendString("File: [");
            USART_sendStringN(Entry.filename, 8);
            USART_sendStringN(Entry.ext, 3);
            USART_sendString("]\n");
    }
    USART_sendString("\tSize: ");
    USART_sendNumber(Entry.file_size);
    USART_sendString(", Attributes: ");
    USART_sendHex(Entry.attributes);
    USART_sendString("\n");
}
