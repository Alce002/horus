#include <stdlib.h>
#include <string.h>

#include "bmp280.h"
#include "shell.h"
#include "twi.h"
#include "usart.h"

struct {
    int32_t t_fine;
    uint16_t T1;
    int16_t T2;
    int16_t T3;
    uint16_t P1;
    int16_t P2;
    int16_t P3;
    int16_t P4;
    int16_t P5;
    int16_t P6;
    int16_t P7;
    int16_t P8;
    int16_t P9;
} comp;

int cmd() {
    char* mode = strtok(NULL, SHELL_DELIM);
    if(strcmp(mode, "help") == 0) {
        USART_sendString("BMP280 interface\n");
        USART_sendString("\thelp\t\t\tPrint this help\n");
        USART_sendString("\tread <adr> <len>\tPrint <len> BMP280 registers starting at <adr>\n");
        USART_sendString("\twrite <adr> <data>\tWrite <data> to BMP280 register <adr>\n");
        USART_sendString("\tstart\t\t\tEnable and configure BMP280\n");
        USART_sendString("\ttemp\t\t\tGet compensated temperature value from BMP280\n");
        USART_sendString("\tpress\t\t\tGet compensated pressure value from BMP280\n");
    } else if(strcmp(mode, "read") == 0) {
        char* arg1 = strtok(NULL, " ");
        char* arg2 = strtok(NULL, " ");
        uint8_t adr = strtol(arg1, NULL, 0);
        uint8_t len = strtol(arg2, NULL, 0);
        uint8_t* data = malloc(sizeof(*data) * len);
        BMP_read_regs(adr, data, len);
        for(int i = 0; i < len; i++) {
            USART_sendHex(adr + i);
            USART_sendString(" := ");
            USART_sendHex(data[i]);
            USART_sendChar('\n');
        }
    } else if(strcmp(mode, "write") == 0) {
        char* arg1 = strtok(NULL, " ");
        char* arg2 = strtok(NULL, " ");
        uint8_t adr = strtol(arg1, NULL, 0);
        uint8_t data = strtol(arg2, NULL, 0);
        BMP_write_reg(adr, data);
    } else if(strcmp(mode, "start") == 0) {
        BMP_config(BMP280_OSRS_P_16, BMP280_OSRS_T_2, BMP280_MODE_NORMAL, BMP280_FILTER_16);
        BMP_load_comp_values();
    } else if(strcmp(mode, "temp") == 0) {
        int32_t temp = BMP_get_temp();
        USART_sendString("BMP temperature: ");
        USART_sendSNumber(temp);
        USART_sendChar('\n');
    }  else if(strcmp(mode, "press") == 0) {
        int32_t press = BMP_get_press();
        USART_sendString("BMP pressure: ");
        USART_sendSNumber(press);
        USART_sendChar('\n');
    }else {
        USART_sendString("Unknown bmp command.\n");
        return 2;
    }
    return 0;
}

void BMP_init() {
    SHELL_register("bmp", &cmd);
}

void BMP_config(uint8_t osrs_p, uint8_t osrs_t, uint8_t mode, uint8_t filter) {
    BMP_write_reg(0xF4, ((osrs_t << 5) | (osrs_p << 2) | mode));
    BMP_write_reg(0xF5, filter << 2);
}

void BMP_read_regs(uint8_t addr, uint8_t* data, uint8_t len) {
    TWI_start(BMP280_ADDR, 0);
    TWI_write(addr);
    TWI_start(BMP280_ADDR, 1);
    for(int i = 0; i < len; i++) {

        if(i == (len - 1)) data[i] = TWI_read(1);
        else data[i] = TWI_read(0);
    }
    TWI_stop();
}

void BMP_write_reg(uint8_t adr, uint8_t data) {
    if(!(adr == 0xE0 || adr == 0xF4 || adr == 0xF5)) return;
    if(adr == 0xF5) data &= 0xFD;

    TWI_start(BMP280_ADDR, 0);
    TWI_write(adr);
    TWI_write(data);
    TWI_stop();
}

void BMP_load_comp_values() {
    uint8_t data[24];
    BMP_read_regs(0x88, data, 24);
    comp.T1 = (uint16_t)(data[1] << 8 | data[0]);
    comp.T2 = (int16_t)(data[3] << 8 | data[2]);
    comp.T3 = (int16_t)(data[5] << 8 | data[4]);
    comp.P1 = (int16_t)(data[7] << 8 | data[6]);
    comp.P2 = (int16_t)(data[9] << 8 | data[8]);
    comp.P3 = (int16_t)(data[11] << 8 | data[10]);
    comp.P4 = (int16_t)(data[13] << 8 | data[12]);
    comp.P5 = (int16_t)(data[15] << 8 | data[14]);
    comp.P6 = (int16_t)(data[17] << 8 | data[16]);
    comp.P7 = (int16_t)(data[19] << 8 | data[18]);
    comp.P8 = (int16_t)(data[21] << 8 | data[20]);
    comp.P9 = (int16_t)(data[23] << 8 | data[22]);
}

int32_t BMP_get_temp() {
    uint8_t data[3];
    BMP_read_regs(0xFA, data, 3);
    int32_t utemp = (((int32_t)data[0] << 12)
                   | ((int32_t)data[1] << 4)
                   | ((int32_t)data[2] >> 4));

    int32_t var1 = (((utemp >> 3) - (comp.T1 << 1)) * comp.T2) >> 11;
    int32_t var2 = (((((utemp >> 4) - comp.T1) * ((utemp >> 4) - comp.T1)) >> 12) * comp.T3) >> 14;
    comp.t_fine = var1 + var2;
    return (comp.t_fine * 5 + 128) >> 8;
}

int32_t BMP_get_press() {
    BMP_get_temp();
    uint8_t data[3];
    BMP_read_regs(0xF7, data, 3);
    int32_t upress = (((int32_t)data[0] << 12)
                    | ((int32_t)data[1] << 4)
                    | ((int32_t)data[2] >> 4));

    int32_t var1 = (comp.t_fine >> 1) - 64000;
    int32_t var2 = (((var1 >> 2) * (var1 >> 2)) >> 11) * comp.P6;
    var2 = var2 + ((var1 * comp.P5) << 1);
    var2 = (var2 >> 2) + ((uint32_t)comp.P4 << 16);
    var1 = (((comp.P3 * (((var1 >> 2) * (var1 >> 2)) >> 13)) >> 3) + ((comp.P2 * var1) >> 1)) >> 18;
    var1 = ((32768 + var1) * (int32_t)comp.P1) >> 15;
    if(var1 == 0) return 0;
    uint32_t p = ((uint32_t)((int32_t)1048576 - upress - (var2 >> 12))) * 3125;
    if(p < 0x80000000) p = (p << 1) / (uint32_t)var1;
    else p = (p / (uint32_t)var1) << 1;
    var1 = (comp.P9 * (((p >> 3) * (p >> 3)) >> 13)) >> 12;
    var2 = ((int32_t)(p >> 2) * comp.P8) >> 13;
    p = (uint32_t)((int32_t)p + ((var1 + var2 + comp.P7) >> 4));
    return p;
}
