#ifndef BMP280_H
#define BMP280_H

#include <stdint.h>

#define BMP280_ADDR 0x77

#define BMP280_OSRS_P_OFF 0x00
#define BMP280_OSRS_P_1 0x01
#define BMP280_OSRS_P_2 0x02
#define BMP280_OSRS_P_4 0x03
#define BMP280_OSRS_P_8 0x04
#define BMP280_OSRS_P_16 0x05

#define BMP280_OSRS_T_OFF 0x00
#define BMP280_OSRS_T_1 0x01
#define BMP280_OSRS_T_2 0x02
#define BMP280_OSRS_T_4 0x03
#define BMP280_OSRS_T_8 0x04
#define BMP280_OSRS_T_16 0x05

#define BMP280_MODE_SLEEP 0x0
#define BMP280_MODE_FORCED 0x01
#define BMP280_MODE_NORMAL 0x03

#define BMP280_FILTER_OFF 0x00
#define BMP280_FILTER_2 0x01
#define BMP280_FILTER_4 0x02
#define BMP280_FILTER_8 0x03
#define BMP280_FILTER_16 0x04

void BMP_init();
void BMP_config(uint8_t osrs_p, uint8_t osrs_t, uint8_t mode, uint8_t filter);
void BMP_read_regs(uint8_t addr, uint8_t* data, uint8_t len);
void BMP_write_reg(uint8_t addr, uint8_t data);
void BMP_load_comp_values();
int32_t BMP_get_temp();
int32_t BMP_get_press();

#endif // BMP280_H
