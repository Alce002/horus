#ifndef SHELL_H
#define SHELL_H

#define SHELL_DELIM " \t\r\n"

typedef int (*SHELL_function)(void);

void SHELL_init();
void SHELL_loop();
int SHELL_launch(char* args);
void SHELL_register(char* name, SHELL_function ptr);
void SHELL_unregister(char* name);

#endif // SHELL_H
