#include <stdlib.h>
#include <string.h>

#include "shell.h"
#include "usart.h"

struct {
    uint8_t used;
    uint8_t size;
    SHELL_function* ptr;
    char** name;
} functions;

int help() {
    USART_sendString("Horus\n");
    USART_sendString("AVR monitoring by Alexander Cedervall\n");
    for(int i = 0; i < functions.used; i++) {
        USART_sendChar('\t');
        USART_sendString(functions.name[i]);
        USART_sendChar('\n');
    }
    return 0;
}

int reg() {
    char* mode = strtok(NULL, SHELL_DELIM);
    if(strcmp(mode, "help") == 0) {
        USART_sendString("Register interaction\n");
        USART_sendString("\thelp\t\t\t\tPrint this help\n");
        USART_sendString("\tread <adr>{,<adr>}\t\tPrint the contents of one or more addresses\n");
    } else if(strcmp(mode, "read") == 0) {
        char* arg = strtok(NULL, ",");
        while(arg != NULL) {
            int16_t adr = strtol(arg, NULL, 0);
            int8_t data = *(volatile uint16_t*)adr;
            USART_sendHex(adr);
            USART_sendString(" := ");
            USART_sendHex(data);
            USART_sendChar('\n');
            arg = strtok(NULL, ",");
        }
    } else {
        USART_sendString("Unknown register command.\n");
        return 2;
    }
    return 0;
}

void SHELL_init() {
    functions.size = 0;
    functions.used = 0;
    functions.ptr = NULL;
    functions.name = NULL;

    SHELL_register("help", &help);
    SHELL_register("reg", &reg);
}

void SHELL_loop() {
    while(1) {
        int i = 0;
        char c;
        char buf[81];
        USART_sendString("> ");
        do {
            c = USART_readChar();

            if(c == '\b') {
                if(i > 0) {
                    USART_sendString("\b \b");
                    buf[--i] = '\0';
                }
            } else if(i < 80) {
                buf[i++] = c;
                USART_sendChar(c);
            }

        } while(c != '\n');
        buf[i] = '\0';
        SHELL_launch(buf);
    }
}

int SHELL_launch(char* args) {
    char* cmd = strtok(args, SHELL_DELIM);
    int ret = -1;
    for(int i = 0; i < functions.used; i++) {
        if(strcmp(cmd, functions.name[i]) == 0) {
            ret = functions.ptr[i]();
            break;
        }
    }
    if(ret != -1) return ret;
    else return 1;
}

void SHELL_register(char* name, SHELL_function ptr) {
    if(functions.used == functions.size) {
        functions.size = (functions.size < 8) ? 8 : functions.size * 2;
        functions.ptr = realloc(functions.ptr, functions.size * sizeof(*(functions.ptr)));
        functions.name = realloc(functions.name, functions.size * sizeof(*(functions.name)));
    }

    functions.ptr[functions.used] = ptr;
    functions.name[functions.used] = name;
    functions.used++;
}

void SHELL_unregister(char* name) {
    for(int i = 0; i < functions.used; i++) {
        if(strcmp(name, functions.name[i]) == 0) {
            functions.used--;
            if(i < functions.used) {
                functions.name[i] = functions.name[functions.used];
                functions.ptr[i] = functions.ptr[functions.used];
            }
            break;
        }
    }
}
