#ifndef SPI_H
#define SPI_H

#include <avr/io.h>

#define SD_ENABLE() (VPORTE_OUT &= ~PIN3_bm)
#define SD_DISABLE() (VPORTE_OUT |= PIN3_bm)

void SPI_init();
uint8_t SPI_write(uint8_t c);

#endif // SPI_H
