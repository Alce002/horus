#include "spi.h"
#include "usart.h"

void SPI_init() {
    // SPI on pin E0-E3
    PORTMUX.TWISPIROUTEA &= ~PORTMUX_SPI0_gm;
    PORTMUX.TWISPIROUTEA |= PORTMUX_SPI0_ALT2_gc;

    SPI0_CTRLA &= ~SPI_ENABLE_bm;

    VPORTE_DIR &= ~PIN1_bm;
    VPORTE_DIR |= PIN0_bm | PIN2_bm | PIN3_bm;

    SPI0_CTRLA |= SPI_MASTER_bm | SPI_ENABLE_bm;
    USART_sendString("SPI initalized\n");
}

uint8_t SPI_write(uint8_t c) {
    SPI0_DATA = c;
    while(!(SPI0_INTFLAGS & SPI_IF_bm)) {;}
    return SPI0_DATA;
}
