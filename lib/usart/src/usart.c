#include <avr/io.h>

#include "usart.h"

static char I_TO_HEX[] = {'0', '1', '2', '3', '4', '5', '6', '7',
                         '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

static void setBaud(int32_t baud) {
    // Ideal baudrate
    int32_t baud_setting = (((8 * ((float)F_CPU / 6)) / baud) + 1) / 2;

    // Correct baudrate
    int8_t sigrow_val = SIGROW.OSC20ERR5V;
    baud_setting *= (1024 + sigrow_val);
    baud_setting /= 1024;

    USART3.BAUD = (int16_t)baud_setting;
}

void USART_init(int32_t baud) {
    // USART on pin E4 & E5
    PORTMUX.USARTROUTEA |= PORTMUX_USART3_ALT1_gc;
    USART3.CTRLB &= ~(USART_TXEN_bm | USART_RXEN_bm);

    // Pin E5 input, pin E4 output
    VPORTB.DIR &= ~PIN5_bm;
    VPORTB.DIR |= PIN4_bm;
    VPORTB.OUT |= PIN4_bm;

    // Standard transmisson speed
    USART3.CTRLB &= ~USART_RXMODE_gm;
    USART3.CTRLB |= USART_RXMODE_NORMAL_gc;

    // 8bit framesize
    USART3.CTRLC &= ~USART_CHSIZE_gm;
    USART3.CTRLC |= USART_CHSIZE_8BIT_gc;

    setBaud(baud);

    USART3.CTRLB |= USART_TXEN_bm | USART_RXEN_bm;

    USART_sendString("USART initalized (");
    USART_sendNumber(baud);
    USART_sendString(")\n");
}

char USART_readChar() {
    char c;
    while(!(USART3.STATUS & USART_RXCIF_bm)) {;}
    c = USART3.RXDATAL;
    if(c == '\r') c = '\n';
    return c;
}

void USART_flush() {
    while(!(USART3.STATUS & USART_DREIF_bm)) {;}
}

void USART_sendChar(char c) {
    if(c == '\n') USART_sendChar('\r');
    while(!(USART3.STATUS & USART_DREIF_bm)) {;}
    USART3.TXDATAL = c;
}

void USART_sendString(const char* str) {
    for(int i = 0; str[i] != '\0'; i++) {
        USART_sendChar(str[i]);
    }
}

void USART_sendStringN(const char* str, int n) {
    for(int i = 0; i < n; i++) {
        USART_sendChar(str[i]);
    }
}

void USART_sendNumber(uint32_t n) {
    if(n == 0) {
        USART_sendChar('0');
        return;
    }
    uint32_t i = 1;
    while(i <= n) i *= 10;
    while(i > 1) {
        i /= 10;
        USART_sendChar((n / i) + '0');
        n %= i;
    }
}

void USART_sendHex(uint32_t n) {
    USART_sendString("0x");
    int i;
    if(n > 0xFFFF) i = 32;
    else if(n > 0xFF) i = 16;
    else i = 8;
    while(i > 0) {
        USART_sendChar(I_TO_HEX[(n >> (i - 4)) & 0xF]);
        i -= 4;
    }
}

void USART_sendSNumber(int32_t n) {
    if(n == 0) {
        USART_sendChar('0');
        return;
    }
    if(n < 0) {
        n = -n;
        USART_sendChar('-');
    }
    int32_t i = 1;
    while(i <= n) i *= 10;
    while(i > 1) {
        i /= 10;
        USART_sendChar((n / i) + '0');
        n %= i;
    }
}
