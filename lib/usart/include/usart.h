#ifndef USART_H
#define USART_H

#include <stdint.h>

void USART_init(int32_t baud);
char USART_readChar();
void USART_flush();
void USART_sendChar(char c);
void USART_sendString(const char* str);
void USART_sendStringN(const char* str, int n);
void USART_sendNumber(uint32_t n);
void USART_sendSNumber(int32_t n);
void USART_sendHex(uint32_t n);

#endif // USART_H
