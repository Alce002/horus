libs=$(wildcard ./lib/*/)
include_dir=-iquote./include/ $(patsubst %,-iquote%include/,$(libs))
lib_dir=$(patsubst %,-L%bin/,$(libs))
link=$(patsubst ./lib/%/,-l%,$(libs))

baud=115200
avrType=atmega4809
avrFreq=20000000L
programmerDev=/dev/ttyACM0
programmerType=jtag2updi

cflags=-DF_CPU=$(avrFreq) -mmcu=$(avrType) -Wall -Werror -Wextra -Os $(include_dir) $(lib_dir)
lflags=$(link)

src=src/
build=build/
bin=bin/

target=$(bin)main.hex
sources=$(wildcard $(src)*.c)
objects=$(patsubst $(src)%.c, $(build)%.o, $(sources))

.PHONY: all flash clean load $(libs)

all: $(libs) $(build) $(bin) $(target)

$(build):
	mkdir -p $@

$(bin):
	mkdir -p $@

$(build)%.o: $(src)%.c
	avr-gcc $(cflags) -c $< -o $@

$(bin)main.elf: $(objects)
	avr-gcc $(cflags) -o $@ $^ $(lflags)

$(target): $(bin)main.elf
	avr-objcopy -O ihex $< $@

flash: all
	stty -F $(programmerDev) 1200
	stty -F $(programmerDev) 1200
	stty -F $(programmerDev) 1200
	avrdude -Cavrdude.conf -p$(avrType) -c$(programmerType) -P$(programmerDev) -b$(baud) -D -e -v -Uflash:w:$(target) -Ufuse2:w:0x02:m -Ufuse5:w:0xc9:m -Ufuse8:w:0x00:m

clean:
	rm -rf bin build
	$(foreach dir,$(libs),$(shell rm -rf $(dir)bin $(dir)build))

$(libs):
	$(MAKE) -C $@ avrType=$(avrType) avrFreq=$(avrFreq)
