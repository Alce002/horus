#!/usr/bin/python3
import serial

with serial.Serial(port='/dev/ttyACM0', baudrate=115200, bytesize=8, timeout=2, stopbits=serial.STOPBITS_ONE) as port:
    port.read_until(b'> ')
    while(1):
        data = input('> ')
        if(data == 'quit'):
            break;
        port.write(bytes(data + '\n', 'utf-8'))
        port.read_until(b'\n')
        print(port.read_until(b'> ').decode('ASCII')[:-3])
